package com.example.homework1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@RestController
@SpringBootApplication
public class Homework1Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework1Application.class, args);
    }

//    @GetMapping("/hellos")
//
//    public String myNames(@RequestParam(value = "myName",defaultValue = "world") String name){
//        return String.format("Hello %s",name);
//    }

}
