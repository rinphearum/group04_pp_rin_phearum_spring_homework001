package com.example.homework1;

import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {
    ArrayList<Customer> customers = new ArrayList<>();
//   static public int i =0;

    public CustomerController() {
        customers.add(new Customer(1,"phearum","male",12,"pp"));
    }




    @PostMapping("/customer/insert")
    public ResponseEntity<?> insertCustomer(@RequestBody Customer customer){
        customers.add(customer);
        return ResponseEntity.ok(new Response<ArrayList<Customer>>(LocalDateTime.now(),200,"Inserted customer successfully!",customers));
    }
    @GetMapping("/customer/search/{id}")
    public  ResponseEntity<?> getCustomerById(@PathVariable Integer id){
        for(Customer cus:customers){
            if(cus.getId() == id){
                return ResponseEntity.ok(new Response<Customer>(LocalDateTime.now(),200,"Founded customer...",cus));

            }
        }
        return null;
    }
    @GetMapping("/customer/search/name")
    public ResponseEntity<?> getCustomerByName(@RequestParam String name){
        for (Customer cus:customers){
            if (cus.getName().equals(name)){
                return ResponseEntity.ok(new Response<Customer>(LocalDateTime.now(),200,"Founded customer...",cus));
            }
        }
        return null;

    }
    @GetMapping("/customers")
    public ResponseEntity<?> getAllCustomer(){
        return ResponseEntity.ok(new Response<ArrayList<Customer>>(
                LocalDateTime.now(),
                200,
                "Get customer successfully!",
                customers
        ));
    }


    @PutMapping("/customer/update/{id}")
    public ResponseEntity<?> updateCustomerById(@PathVariable Integer id,@RequestBody UserRequest userRequest){
        int i=0;
        for(Customer cus:customers){
            if(cus.getId() == id){
                cus.setName(userRequest.getName());
                cus.setGender(userRequest.getGender());
                cus.setAge(userRequest.getAge());
                cus.setAddress(userRequest.getAddress());
                customers.set(i,cus);
                return ResponseEntity.ok(new Response<ArrayList<Customer>>(LocalDateTime.now(),200,"Update successfully",customers));
            }


            i++;

        }
        return ResponseEntity.ok(new Response<ArrayList<Customer>>(LocalDateTime.now(),500,"Can not update",customers));
    }

    @DeleteMapping("/customer/delete/{id}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable Integer id){
        for(Customer cus:customers){
            if(cus.getId() == id){
                customers.remove(cus);
                return ResponseEntity.ok(new Response<ArrayList<Customer>>(LocalDateTime.now(),200,"Deleted customer successfully",customers));
            }

        }
        return ResponseEntity.ok(new Response<ArrayList<Customer>>(LocalDateTime.now(),500,"Can not update delete customer",customers));

    }


}
